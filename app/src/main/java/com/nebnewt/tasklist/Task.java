package com.nebnewt.tasklist;

/**
 * Created by Neb on 18.11.2016.
 */

public class Task {
    private String description;
    private String name;
    private boolean isFinished;
    private int importance;

    public Task(String name, String description, int importance)
    {
        this.isFinished=false;
        this.name=name;
        this.description=description;
        this.importance=importance;
    }

    public String s()
    {
        return name+" "+description+" "+importance;
    }
    public int getImportance() {
        return importance;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }
}
